import React, {Component} from 'react';
import {Link} from "react-router-dom";

class PersonView extends Component {

    constructor(props){
        super(props);
        this.state = {
            isEditing : false,
        }
    }

    handleClick = ()=>{
        this.props.delete(this.props.person);
    };


    render() {
        if (this.state.isEditing === false) {
            return (
                <div className={"container"}>
                    <div className={"row"}>
                        <div className={"col-3"}><Link to={"/edit/"+this.props.person.id+"/"+this.props.person.firstname+"/"+this.props.person.lastname}>{this.props.person.id}</Link></div>
                        <div className={"col-3"}>{this.props.person.firstname}</div>
                        <div className={"col-3"}>{this.props.person.lastname}</div>
                        <div className={"col-1"}>
                            <button onClick={this.handleClick} type={"submit"} className={"btn btn-danger"}>delete
                            </button>
                        </div>
                    </div>

                </div>
            );
        }

    }
}

export default PersonView;
