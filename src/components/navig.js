import React, {Component} from 'react';
import Edit from "./Edit";
import App from "../App"
import {BrowserRouter, Link, Route} from "react-router-dom";
import PersonAdd from "./Person-add";

class Navig extends Component {
    render() {
        return (
            <div>
            <BrowserRouter>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <div className="nav-item"><Link className="nav-link" to={"/"}>Home</Link></div>
                    <div className="nav-item"><Link to={"/add"}>Add</Link></div>
                </nav>
                <Route exact path="/" component={App}/>
                <Route path="/add" component={PersonAdd}/>
                <Route path="/edit/:id/:name/:surname"  component={Edit}/>
            </BrowserRouter>

            </div>
        );
    }
}

export default Navig;
