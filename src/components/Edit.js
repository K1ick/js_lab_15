import React, {Component} from 'react';
import {Redirect, useParams} from "react-router";
import {Link} from "react-router-dom";

function Edit() {
    let some='';
    let {id, name, surname} = useParams();
    console.log(name);
    let iname='';
    let isurname='';
        return (
            <div>
                <div className="container">
                    <div className="row" >

                            <div className="col-12">
                                <input type="text" className="form-control" defaultValue={name} ref={(input) => iname = input} required/>
                            </div>
                            <div className="col-12">
                                <input type="text" className="form-control" defaultValue={surname} ref={(input) => isurname = input} required/>
                            </div>
                            <div className="col-12">
                                <Link to={"/"}><button type="submit" className="btn btn-primary mt-2" onClick={()=>SaveEdit(id, name.value, surname.value)}>Сохранить</button></Link>
                            </div>

                    </div>
                </div>
            </div>
        );
}

export default Edit;

async function SaveEdit(id, name, surname){
    fetch('http://localhost:3001/persons/'+id, {
        method: 'PUT',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',

        },
        redirect: 'follow',
        referrer: 'no-referrer',
        body: JSON.stringify({
            'firstname':name,
            'lastname':surname,
            'id':id
        }),
    })
        .then(response => response.json());
}
